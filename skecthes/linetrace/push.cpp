#include"config.h"
#include"push.h"
#include"std.h"

//ノイズキャンセリング関数
void ncancel(int* pl,int* pc,int *pr){
  //ポインタの値を代入
  //(k-2)の入力値
  static int bbcl=0;
  static int bbcc=0;
  static int bbcr=0;
  //(k-1)の入力値
  static int bcl=0;
  static int bcc=0;
  static int bcr=0;
  
  int cl=*pl;
  int cc=*pc;
  int cr=*pr;

  //平均をとるポインタ変数に代入
  *pl=(bbcl+bcl+cl)/3;
  *pc=(bbcc+bcc+cc)/3;
  *pr=(bbcr+bcr+cr)/3;

  //値を代入
  bbcl=bcl;
  bbcc=bcc;
  bbcr=bcr;

  bcl=*pl;
  bcc=*pc;
  bcr=*pr;
}


//押し出し処理用操作量決定関数
int p_det(void){
  //近接センサ情報取得
  int cl=analogRead(analog_left);
  int cc=analogRead(analog_center);
  int cr=analogRead(analog_right);

  //ノイズキャンセリング関数を使用
  ncancel(&cl,&cc,&cr);
  
  //bool型に代入
  bool bl=(cl>thresh);
  bool bc=(cc>thresh);
  bool br=(cr>thresh); 
  //値を確認
  Serial.println("近接センサ");
  Serial.print(cl);
  Serial.print(",");
  Serial.println(bl);

  Serial.print(cc);
  Serial.print(",");
  Serial.println(bc);

  Serial.print(cr);
  Serial.print(",");
  Serial.println(br);

  //カウンタ変数
  static int cnt=0;
  //指令値
  int cmd;
  //初期設定終了
  
  if(bl && bc && br){ //全部検出したとき
    cnt=0;  //カウンタ初期化
    cmd=0;  //前進
  }
  else if(bl==1 && br==0){
    cnt++;
    cmd=-1; //左旋回
  }
  else if( !bl && (br || !(bc||br)) ){
    cnt++;
    cmd=1;  //右旋回
  }
  else{
    cnt++;
    cmd=-256; //何もしない
  }

  if(cnt>plimit){ //カウンタが一定値を超えたら
    cmd=256;  //探索終了
  }
  return cmd; //指令値を出力
}

//モータ駆動関数
int p_cnt(int l2,int l1,int c,int r1,int r2,int cmd){
  int flg=0;  //探索終了フラグ
  int sum= l2 + l1 + c + r1 + r2;  //黒線検出値(!0の時検出)
  //int dr=(l2 + l1)*(-1) + r1 + r2;  //黒線の検出された向き(マイナスの時左,プラス時右にあり，0の時正面)
  
  if(cmd==256){ //探索終了しているなら
    flg=1;   //探索終了フラグを１
  }
  else if(cmd==-256){
    delay(delta); //一定時間待ち
  }
  else if(sum!=0){  //黒線検出
    mc(-256,-256);
    delay(delta); //後退
  }
  else if(cmd==-1){
    mc(-256,256); //左回転
    delay(delta);
  }
  else if(cmd==0){
    mc(256,256);  //前進
    delay(delta);
  }
  else{
    mc(256,-256); //右回転
    delay(delta);
  }

  return flg; //終了フラグを返す
}
