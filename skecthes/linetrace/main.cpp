#include"config.h"
#include"std.h"
#include"push.h"
#include"linetrace.h"
#include"main.h"

void setup() {
  Serial.begin(9600);
  //ライントレース用の入力定義
  pinMode(lt_r2,INPUT);
  pinMode(lt_r1,INPUT);
  pinMode(lt_c,INPUT);
  pinMode(lt_l1,INPUT);
  pinMode(lt_l2,INPUT);
  //定義終了

  //モータ駆動用の出力定義(D1)
  pinMode(d1_l,OUTPUT);
  pinMode(d1_r,OUTPUT);
  //定義終了
}

void loop() {
  //黒線検出(反射すると０，しないと１）
  int r_2=digitalRead(lt_r2);
  int r_1=digitalRead(lt_r1);
  int c_1=digitalRead(lt_c);
  int l_1=digitalRead(lt_l1);
  int l_2=digitalRead(lt_l2);

  int cmd;  //指令値
  int flg;  //フラグ


  //入力反転(flg=1の時反転)
  if(flg){
  r_2=abs(r_2-1);
  r_1=abs(r_1-1);
  c_1=abs(c_1-1);
  l_1=abs(l_1-1);
  l_2=abs(l_2-1);
  }
  
  //test]
  /*
  Serial.println("test");
  Serial.println(r_2);
  Serial.println(r_1);
  Serial.println(c_1);
  Serial.println(l_1);
  Serial.println(l_2);
  */
    
  static int trg=1; //モード切替用の状態変数
  //trg=trig(l_2,l_1,c_1,r_1,r_2);  //モード切替関数

  if(trg<1){  //ライントレース用処理開始
  double cval=l_det(l_2,l_1,c_1,r_1,r_2);   //操作量を取得
  //Serial.println(cval);
  l_cnt(cval);

  } //ライントレース用処理終了

  //押し出し処理開始
  else if(trg<2){   
    cmd=p_det();
    flg=p_cnt(r_2,r_1,c_1,l_1,l_2,cmd);
    Serial.println("test:");
    Serial.println(cmd);
    Serial.println(flg);
  } 
  //押し出し処理終了
  
  //出口探索処理開始
  else{ 


    
  }
  //出口探索処理終了
  
  delay(delta);

}
