#include"config.h"
#include"std.h"

void mc(int lv,int rv){
  //モータ駆動用関数
  //それぞれの引数は-255~+255までとり,プラスなら正転,マイナスなら逆転,ゼロなら停止
  //返り値はなし
  
  //左モータ
  if(lv>0){   //正転させたいとき
    digitalWrite(d1_l,HIGH);
    analogWrite(pwm_l,abs(lv));    //0から255の整数をとる
  }
  else if(lv<0){  //逆転させたいとき
    digitalWrite(d1_l,LOW);
    analogWrite(pwm_l,abs(lv));
  }
  else{
    analogWrite(pwm_l,0);   //デューティー比0にして,静止
  }

  //右モータ
    if(rv>0){   //正転させたいとき
    digitalWrite(d1_r,HIGH);
    analogWrite(pwm_r,abs(rv));    //0から255の整数をとる
  }
  else if(rv<0){  //逆転させたいとき
    digitalWrite(d1_r,LOW);
    analogWrite(pwm_r,abs(rv));
  }
  else{
    analogWrite(pwm_r,0);   //デューティー比0にして,静止
  }

//実験用ダミー
Serial.print("left:");
Serial.println(lv);
Serial.print("right:");
Serial.println(rv);
//ダミー終了
}
