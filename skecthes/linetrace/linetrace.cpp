#include"config.h"
#include"std.h"
#include"linetrace.h"

double l_det(int l2,int l1,int c,int r1,int r2){
  //引数の値は0(LOW)か1(HIGH)となる
  //1(HIGH)の時黒線上，0(LOW)の時白線となる．
  //返り値として-128~+128の値を返す.
  //これはマイナスだと左旋回,プラスだと右旋回,0だと直進を意味する.
  /*
   * 各センサ位置に応じて重みづけを行う．
   * 右側に黒線があるときはプラス
   * 左側に黒線があるときはマイナス
   * 真ん中は０
   * それらを処理して操作量を決定する．
   * ただし，中央に黒線があったときは急激な動作を減らすために，
   * 操作量の絶対値を少し小さくする．
   */
  //Serial.print("test:");
  //Serial.println(r2);
  //入力を確認する
  double sum=r2+r1+c+l1+l2;  //現在の反応したセンサの数0~5の値をとる.
  static int pvalue;        //一つ前の入力要素
  pvalue=0;
  double value;                //現在の入力要素
  //センサーに反応があった場合,右ならプラス,左ならマイナスの値をとるようにする
  if(sum>0){  
    value=(r2*2)+(r1*1)+(l1*-1)+(l2*-2);
    value=value/sum;
  }
  else{  //sum=0の時，センサに反応がなかった時
  value=0;
  }

  if(c==1){
    value=value*0.8;  //操作量を減らす.
  }

  value=64.0*value;
  pvalue=value;    //値を格納する 
  return value;   //値を出力
}




void l_cnt(double val){
  //double:det(int)関数で得られた操作量を入力し,モータ関数void:mc()を操作する.
  //引数として-128~128の値をとる

  //暫定的な関数
  //val>0の右折
  //val<0左折
  if(val>0){  //右折の時
    mc(255,0);
  }
  else if(val<0){
    mc(0,255);
  }
  else  //val==0の時
  mc(255,255);

  //暫定的な関数作成終了
  
}
