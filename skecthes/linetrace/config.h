#pragma once
#include "Arduino.h"

//ピン番号割り振り(ライントレース用)
const int lt_r2=4;   //一番右
const int lt_r1=7;
const int lt_c=8;   //真ん中
const int lt_l1=12;
const int lt_l2=13;    //一番左

//ピン番号割り振り(モータ駆動用(D1))
const int d1_l=5;   //左
const int d1_r=6;   //右

//ピン番号割り振り(モータ駆動用(PWM))
const int pwm_l=10;   //左 
const int pwm_r=11;   //右

//ピン番号割り振り（近接センサ用)
const int analog_left=0;
const int analog_center=1;
const int analog_right=2;
//割り振り設定終了

//更新間隔設定
const int delta=300;

//近接センサ用閾値(閾値以上ならあると判断）
const int thresh=50;

//押し出し処理用カウンタ上限
const int plimit=100;

//入力反転用フラグ
const bool flg=0;
