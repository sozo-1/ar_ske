#include"config.h"
#include"std.h"
#include"search.h"

/*
出口探索用操作量決定関数
壁伝いに反時計回りに回る
引数：黒線検出
返り値:指令値cmdを返す
cmd
0:壁にぶつかるまで前進処理
11:前進処理
12:左旋回処理
13:右旋回処理
14:ゆるく右旋回
3:ライントレースに切り替える処理
4:終了
*/

int s_det(int l2,int l1,int c,int r1,int r2,int r3){
    //黒線の値を確認
    int sum=l2+l1+c+r1+r2+r3;
    static int psum=0;  //過去のsum
    //直進して黒線を検出したとき,l1かr1が最初に検知されるはず
    //そうでないとき,出口だと判断する
    bool l_sum=l2+l1;    //左側と中央の黒線検出
    bool r_sum=c+r1+r2+r3;    //右側の黒線検出
    static int l_count=0;   //
    static int w_cnt=0;     //白線カウンタ
    static int cnt=0;       //case3のカウンタ
    static int br1=0;      //ひとつ前のr1
    static int bl1=0;      //ひとつ前のl1
    static int bbr1=0;      //さらにひとつ前のr1
    static int bbl1=0;      //さらにひとつ前のl1
    static int bc=0;
    static int bbc=0;
    //カウンタ開始して数秒間はコマンド11を続ける
    static unsigned long time=millis();
    const unsigned long Limit=3000;   //コマンド11を維持する時間[ms]
    const unsigned long eLimit=23000;      //出口前の直線に差し掛かる時間[ms]

    static int cmd=11;      //状態変数（初期値11)
    static int sw=0;
    //cmd=1*の時の処理
    //左側のセンサに黒線を検出したらcase3に移行
    //それ以外は,右手を壁伝いにジグザグ運動を行う.(要操作量調整)
    if(sum==6){
        cmd=12;
    }
    else if(cmd==0){     //cmd=0の時
        if(!sum&&!psum){   //黒線を検出しないとき
        cmd=0;      //前進
        }
        else{
        cmd=2;      //反時計回り
        }
    }
    else if(cmd>=10){        //
        //黒線を検知しなかったとき
        if(!sum){w_cnt++;}       //カウンタ+1
        else{w_cnt=0;}  //カウンタ初期化
        //内部の処理
        if(cmd==11){   //cmd=11の時
            if(r_sum && millis()-time<eLimit){
                cmd=12;
            }
            else if(r_sum && millis()-time>=eLimit){
                cmd=14;
            }
            else if(l_sum && millis()-time>eLimit){     //左に黒線を検知したとき
                cmd=3;
            }
            else if(w_cnt>slimit && sum==0){
                //cmd=13;
                w_cnt=0;
            }
        }
        //cmd=12の時,右手に黒線を検知しなかったとき直進運動に移行
        //そうでなければcmd12の左旋回を維持
        else if(cmd==12 && !r_sum){
            cmd=11;
        }
        else if(cmd==14 && !r_sum){
            cmd=11;
        }
        //cmd=13の時
        else if(cmd==13){   //一定時間時計回りに回った後，cmd=11とする
            cmd=11;
        }
    }

    //cmd3の時
    else if(cmd==3){           
        //黒線を中央付近に検出したら落下防止のため中断
        if((bbr1||br1||r1) && (bbc||bc||c)){  
            cmd=12;
        }
        else if(cnt<s3limit){ //一定カウント内かつ,黒線が検知されたとき
        cnt++;
        Serial.print("cnt:");
        Serial.println(cnt);
        }
        /*
        else if((psum!=0||sum!=0) && cnt<s3limit){      //黒線が検知されないとき
        cmd=11; //case11に戻る
        cnt=0;  //カウンタ初期化
        }
        */
        else{    //黒線が検知されかつ,カウントを超えたとき
        cmd=4;  //case4へ移行
        Serial.println("case4へ");
        }
    }
    //cmd=2の時
    else if(cmd==2){
        if(!(l2||l1)&&(r3)){       //黒線を検出しないとき
        cmd=11;         //壁伝い処理に移行
        }
        else{cmd=2;}
    }
    //test
    Serial.print("sum:");
    Serial.println(sum);
    Serial.print("command:");
    Serial.println(cmd);
    Serial.print("w_cnt");
    Serial.println(w_cnt);
    psum=sum;
    //過去の値を収納
    bbr1=br1;     
    bbl1=bl1;
    bbc=bc;
    br1=r1;     //現在のr1を代入
    bl1=l1;     //現在のl1を代入
    bc=c;
    return cmd;
}


//出口探索処理用モータ駆動関数
int s_cnt(int cmd){
    int s_flg=0;
    if(cmd==0){ //case0の時
    mc(255,255);    //前進
    }
    else if(cmd==2){
        mc(-100,100);
    }
    else if(cmd==11){    //case11の時
        mc(255,160);    //ゆるく右曲がり
    }
    else if(cmd==12){
        mc(-80,0);   //反時計回り
    }
    else if(cmd==13){
        int i=0;
        while(i<150){
        //mc(255,-200);   //時計回り
        i+=delta;
        }
    }
    else if(cmd==14){
        mc(80,230);    //ゆるく左曲がり
    }
    else if(cmd==3){
        mc(100,255);    //ゆるく左折
    }
    else if(cmd==4){
        s_flg=1;        //終了フラグを返す
    }
    return s_flg;
}
