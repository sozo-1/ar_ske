#include"config.h"
#include"std.h"
#include"linetrace.h"

int l_cnt(double val,double ac){
  //double:pid(int)関数で得られた操作量を入力し,モータ関数void:mc()を操作する.
  //引数として-128~128の値をとる
  //減衰係数acをかけることで速度を変更する
  if(val== 500){
    return 1;   //次の処理に移行
  }else{
  if(abs(val)>128){   //値の絶対値が128を超えていたら128に丸める
      if(val>128){
        val=128;
      }
      else{
        val=-128;
      }
  }
  
  //暫定的な関数
  //val>0の右折
  //val<0左折
  if(val>0){
    //右の操作量を決定
    double rc=-500.0/128.0*val+255.0;
    mc(255*abs(ac),abs(ac)*rc);
  }
  else if(val<0){
    //左の操作量を決定
    double lc=500.0/128.0*val+255.0;
    mc(abs(ac)*lc,abs(ac)*255);
  }
  else{   //val=0の時前進
    mc(abs(ac)*255,abs(ac)*255);
  }
  //暫定的な関数作成終了
  }
  return 0;
}

//エラーチェック関数（検出された値が連続でないとき１を返す．)
int l_chk2(int* vec){
  int er1=0;  //立ち上がりエッジ(01...を検出)
  int er2=0;  //立下りエッジ(10...を検出)
  
  for(int i=0;i<nvec-1;i++){
    //立ち上がりエッジを検出する
    //立上がりエッジがあり，立下りがないとき 
    if((!vec[i]&&vec[i+1])&&er2==0){  
      er1++;  //1加算
    }
    //すでに立下りエッジを検出しているとき
    else if((!vec[i]&&vec[i+1])&&er2==1){
      return 1;   //エラーを出力
    }
    //立下りエッジを検出
    if(vec[i]&&!vec[i+1]){
      er2++;  //1加算
    }
  }
return 0;
}

//pid制御
double pid(int l2,int l1,int c,int r1,int r2){
  const double KP=2.8;  //比例係数
  const double KI=0.03;  //積分係数
  const double KD=0.01;  //微分係数
  const double Ts=delta*0.001;  //制御周期
  double sum=l2+l1+c+r1+r2;
  const double tvalue=0;  //指令値
  double value;
  static double e[3]={0,0,0}; //偏差を格納
  double delta_u;   //操作量の微小変化量
  static double u[2]={0,0}; //操作量
  //例外処理用
  static int wcnt=0;  //白線カウンタ
  const int Wmax=8;  //カウンタ上限
  
  //valueを計算
  value = l2*-4 + l1*-2+ r1*2 + r2*4;
  if(sum==0){   //何も検知しないとき時計回り
    //value=0;
    wcnt++; //カウンタ加算
    if(wcnt<Wmax){
      return 128;
    }
    else{   //カウンタ上限を超えたときモード切替
      return 500; //モード切替
    }
  }
  else{
    value=value/sum;
    wcnt=0; //カウンタ初期化
  }
  //エラーがあった場合再計算を行う
  //データをベクトルに格納
  int vec[]={l2,l1,c,r1,r2};
  if(l_chk2(vec)){  //エラーがあったとき
    sum=l1+c+r1;    //中央付近のセンサのみを使う
    value=  l1*-1.5 +  r1*1.5;  //重みづけを変更
    if(sum==0){ //sumがゼロならvalue=0
      value=0;
    }  
    else{
      value=value/sum;
    }
  }
  //エラー処理終了
  
  //速度pid
  //偏差を計算
  e[0]=value-tvalue;
  //delta_uを計算
  delta_u=KP*(e[0]-e[1]  + KI*Ts*e[0]  + KD/Ts*(e[0]-2*e[1]+e[2]) );
  u[0]=u[1]+delta_u;
  //値の更新
  u[1]=u[0];
  e[2]=e[1];
  e[1]=e[0];
  
  Serial.print("value:");
  Serial.println(value);
  Serial.print("pid:");
  Serial.println(u[0]*8.0); 

  return (u[0]*8.0);    //このままでは小さいので定数倍して正規化
}

//帰りのPID処理，行きと異なるパラメータを持つ
double pid2(int l2,int l1,int c,int r1,int r2){
  const double KP=2.68;  //比例係数
  const double KI=0.01;  //積分係数
  const double KD=0.01;  //微分係数
  const double Ts=delta*0.001;  //制御周期
  double sum=l2+l1+c+r1+r2;
  const double tvalue=0;  //指令値
  double value;
  static double e[3]={0,0,0}; //偏差を格納
  double delta_u;   //操作量の微小変化量
  static double u[2]={0,0}; //操作量
  //例外処理用
  static int wcnt=0;  //白線カウンタ
  const int Wmax=10;  //カウンタ上限
  
  //valueを計算
  value = l2*-4.0 + l1*-2.0 + r1*2.0 + r2*4.0;
  if(sum==0){   //何も検知しないとき時計回り
    //value=0;
    wcnt++; //カウンタ加算
    if(wcnt<Wmax){
      return -128;
    }
    else{   //カウンタ上限を超えたときモード切替
      return 500; //モード切替
    }
  }
  else{
    value=value/sum;
    wcnt=0; //カウンタ初期化
  }
  //エラーがあった場合再計算を行う
  //データをベクトルに格納
  int vec[]={l2,l1,c,r1,r2};
  if(l_chk2(vec)){  //エラーがあったとき
    sum=l1+c+r1;
    value=  l1*-1 +  r1*1;  //中央付近だけ使う
    if(sum==0){ //sumがゼロならvalue=0
      value=0;
    }  
    else{
      value=value/sum;
    }
  }
  //エラー処理終了
  
  //速度pid
  //偏差を計算
  e[0]=value-tvalue;
  //delta_uを計算
  delta_u=KP*(e[0]-e[1]  + KI*Ts*e[0]  + KD/Ts*(e[0]-2*e[1]+e[2]) );
  u[0]=u[1]+delta_u;
  //値の更新
  u[1]=u[0];
  e[2]=e[1];
  e[1]=e[0];
  
  Serial.print("value:");
  Serial.println(value);
  Serial.print("pid:");
  Serial.println(u[0]*8.0);

  return (u[0]*8.0);
}
