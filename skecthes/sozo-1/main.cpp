//ヘッダファイルをインクルード
#include"config.h"
#include"std.h"
#include"linetrace.h"
#include"search.h"
#include"main.h"

void setup() {
  Serial.begin(9600);
  //ライントレース用の入力定義
  pinMode(lt_r3,INPUT);
  pinMode(lt_r2,INPUT);
  pinMode(lt_r1,INPUT);
  pinMode(lt_c,INPUT);
  pinMode(lt_l1,INPUT);
  pinMode(lt_l2,INPUT);
  //定義終了

  //モータ駆動用の出力定義(D1)
  pinMode(d1_l,OUTPUT);
  pinMode(d1_r,OUTPUT);
  //定義終了
}

void loop() {
  
  //黒線検出(反射すると０，しないと１）
  int r_3=digitalRead(lt_r3);
  int r_2=digitalRead(lt_r2);
  int r_1=digitalRead(lt_r1);
  int c_1=digitalRead(lt_c);
  int l_1=digitalRead(lt_l1);
  int l_2=digitalRead(lt_l2);
  int cmd;  //指令値

  //入力反転(flg=1の時反転)
  if(r_flg){
    r_3=abs(r_3-1);
    r_2=abs(r_2-1);
    r_1=abs(r_1-1);
    c_1=abs(c_1-1);
    l_1=abs(l_1-1);
    l_2=abs(l_2-1);
  }
  //test
  Serial.println("test");
  Serial.println(r_3);
  Serial.println(r_2);
  Serial.println(r_1);
  Serial.println(c_1);
  Serial.println(l_1);
  Serial.println(l_2);
  //trgの割り振り
  //0:ライントレース
  //1:出口探索
  //2:帰りのライントレース
  
  static int trg=0; //モード切替用の状態変数
  trg=trg%3;  //0,1,2,0,1,2....と状態変数が循環するようにする

  if(trg<1){  //ライントレース用処理開始
  double cval=pid(l_2,l_1,c_1,r_1,r_2); //PID制御を行う
  trg+=l_cnt(cval,1.0);
  } //ライントレース用処理終了

  //出口探索処理開始
  else if(trg<2){ 
    cmd=s_det(l_2,l_1,c_1,r_1,r_2,r_3);
    trg+=s_cnt(cmd);
  }
  //出口探索処理終了

  //帰りのライントレース処理開始
  else{
    static unsigned long dtime=millis(); //下り坂タイマー[ms]
    double cval=pid2(l_2,l_1,c_1,r_1,r_2);   //操作量を取得
    if(millis()-dtime<Dht1){    //時間内なら
      trg+=l_cnt(cval,Dhac2);  //減衰動作
      //dtime=dtime+delta;  //deltaを加算
      Serial.println("最初のゆっくり");
    }
    else if(millis()-dtime<Dht2){      
      trg+=l_cnt(cval,Dhac1);
      Serial.println("次のゆっくり");
    }
    else{
      trg+=l_cnt(cval,1.0); 
      Serial.println("通常動作");
    }
    Serial.print("カウント:");
    Serial.println(millis()-dtime);
  }
  //帰りのライントレース処理終了
  delay(delta);
}
