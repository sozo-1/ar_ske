#pragma once
//ライントレース用モータ駆動用関数:引数第2項は減衰係数(0<ac<=1)の値をとる
int l_cnt(double val,double ac);
//ライントレース用エラーチェック関数(検出された値が連続でないとき１を返す．)
int l_chk2(int* vec);
//行きのライントレース
double pid(int l2,int l1,int c,int r1,int r2);
//帰りのライントレース
double pid2(int l2,int l1,int c,int r1,int r2);

