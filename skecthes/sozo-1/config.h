#pragma once
#include "Arduino.h"

//ピン番号割り振り(ライントレース用)
const int lt_r3=4;
const int lt_r2=7;
const int lt_r1=8;
const int lt_c=12;
const int lt_l1=13;
const int lt_l2=9;

//ピン番号割り振り(モータ駆動用(D1))
const int d1_l=5;   //左
const int d1_r=6;   //右

//ピン番号割り振り(モータ駆動用(PWM))
const int pwm_l=10;   //左 
const int pwm_r=11;   //右

//ピン番号割り振り（近接センサ用)
const int analog_left=0;
const int analog_center=1;
const int analog_right=2;
//割り振り設定終了

//ライントレース用センサ数
const int nvec=5;

//更新間隔設定
const int delta=10;

//入力反転用フラグ
const bool r_flg=0;

//出口探索処理用(カウンタ上限)
const int slimit=30;
const int s3limit=3;

//帰りのライントレース用遅くする時間単位は[ms]
const unsigned long Dht1=7000;
const unsigned long Dht2=14000;

//帰りのライントレース用減衰係数(0<dhac<=1)
const double Dhac1=0.7;
const double Dhac2=0.3;